
def decorator_maker(arg1, arg2):
    print '!!!!!!decorator maker works first', arg1, arg2

    def my_decorator(func):
        print "!!!my decorator works first", arg1, arg2

        def wrapped(func_arg1, func_arg2):
            print '!wrapper works first', arg1, arg2, func_arg1, func_arg2
            return func(func_arg1, func_arg2)

        return wrapped

    return my_decorator


@decorator_maker('-=dec 1=-', '-=dec 2=-')
def func(func_arg1, func_arg2):
    print 'i am func and i got {}, {}'.format(func_arg1, func_arg2)


func('-=func 1=-', '-=func 2=-')