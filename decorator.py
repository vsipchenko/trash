import datetime


def decorator(func):
    def wrapper(*args):
        beafore = datetime.datetime.now()

        func(*args)

        after = datetime.datetime.now()
        res = after - beafore
        res = '{}.{} sec'.format(res.seconds, res.microseconds)
        print res
    return wrapper


@decorator
def func1(lst):
    res = []
    for i in lst:
        res.append(i * 2)
    return res


@decorator
def func2(lst):
    return map((lambda i: i * 2), lst)


lst = range(12, 48, 2)
func1(lst)
func2(lst)

