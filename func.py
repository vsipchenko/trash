class Person(object):
    def __init__(self, name):
        self.name = name

    def show_data(self):
        return self.name


class Student(Person):

    def __init__(self, education):
        super(Person).__init__()
        self.education = education


class Worker(Person):
    def __init__(self, work_place):
        super(Person).__init__()
        self.work_place = work_place


class Academy(object):
    person_list = []

    def show_all(self):
        for person in self.person_list:
            print 'Name: {}'.format(person.show_data)

    def add_person(self, person):
        if isinstance(person, Person):
            self.person_list.append(person)

