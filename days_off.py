from datetime import date, timedelta


def days_off(date1, date2):
    res = {}
    while date1 <= date2:
        if date1.weekday() in (5, 6):
            res.update({date1: date1.weekday()})
        date1 += timedelta(days=1)
    return len(res)


date1 = date(2016, 10, 1)
date2 = date(2016, 11, 5)
print days_off(date1, date2)
