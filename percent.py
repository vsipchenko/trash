import re


def percent(string):
    string = string.lower()
    lst = re.findall(r'\w', string)
    result = {}
    for i in lst:
        percent = lst.count(i) * 100 / len(lst)
        result.update({i: percent})
    return result


print percent('aat, AAAAAA bbbbt. ... et y u o ')
