def are_vowel(letter1, letter2):
    if letter1 in 'aeyuoi' or letter2 in 'aeyuoi':
        return True
    return False


def are_marks(letter1, letter2):
    if not letter1.isalpha() or not letter2.isalpha():
        return True
    return False


def find_a(string):
    string = string.lower()
    i = 0
    res = []
    while i < len(string) - 1:
        i += 1
        if string[i] == 'a' \
                and not are_vowel(string[i - 1], string[i + 1]) \
                and not are_marks(string[i - 1], string[i + 1]):
            res.append({string[i]: i})
    print res
    return len(res)


print find_a('a ia ai iai pap ppapp bbbabbaabb paap ppa ,a, ,a a,')
