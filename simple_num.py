import math
lst_of_num = range(1, 10002)


def is_simple(num):
    for i in range(2, int(math.sqrt(num) + 1)):
        if not num % i:
            return False
    return True


def display_simple(num):
    lst_of_simple_num = []
    cur_num = 1
    while len(lst_of_simple_num) < num:
        cur_num += 1
        if is_simple(cur_num):
            lst_of_simple_num.append(cur_num)
    print lst_of_simple_num[-1]
    return lst_of_simple_num[-1]


display_simple(10001)


# if __name__ == "__main__":
#     print is_simple(4)