from datetime import date, timedelta


def friday(date1, date2):
    res = []
    while date1 <= date2:
        if date1.day == 13 and date1.weekday() == 4:
            res.append(date1)
        date1 += timedelta(days=1)
    print res
    return len(res)


date1 = date(2010, 1, 1)
date2 = date(2020, 1, 1)
print friday(date1, date2)