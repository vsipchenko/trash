romanList = [
    (1000, "M"), (900, "CM"),
    (500, "D"), (400, "CD"),
    (100, "C"), (90, "XC"),
    (50, "L"), (40, "XL"),
    (10, "X"), (9, "IX"),
    (5, "V"), (4, "IV"),
    (1, "I")
]


def to_roman(integer):
    result = []
    parenscount = 0
    while integer:
        integer, num = divmod(integer, 4000)
        roman_result = ""
        for value, rchar in romanList:
            count, num = divmod(num, value)
            roman_result += count * rchar
        if roman_result:
            result.append('{}{}{}'.format(
                '(' * parenscount, roman_result, ')' * parenscount))
        parenscount += 1
    return ' '.join(result[::-1])

print to_roman(9)






# num = int(raw_input('>>> '))
#
#
# # only till 39
#
#
# def convert_to_rim(num):
#     rim_num = {1: 'I', 5: 'V', 10: 'X'}
#     res = ''
#     if num <= 0 or num >= 40:
#         return False
#     if num >= 10:
#         res = (num / 10) * rim_num[10]
#     if num % 10 >= 5:
#         if num % 10 == 5:
#             res += rim_num[5]
#         if num % 10 == 9:
#             res += rim_num[1] + rim_num[10]
#         else:
#             res += rim_num[5] + rim_num[1] * (num % 10 - 5)
#     if num % 10 < 5:
#         if num % 10 == 4:
#             res += rim_num[1] + rim_num[5]
#         else:
#             res += rim_num[1] * (num % 10)
#     return res
#
#
# print convert_to_rim(num)


# def func(num):
#     rim_num = {1: 'I', 4: 'IV', 5: 'V', 9: 'IX', 10: 'X', 50: 'L', 100: 'C'}
#     hundred = num / 100
#     fifty = 50 % (num - hundred * 100)
#     ten = (num - fifty * 50) % 10
#     five = (num - ten * 10) % 5
#     one = (num - five)
#     print hundred, fifty, ten, fifty, one
#     res = (hundred * rim_num[100]) + (fifty * rim_num[50]) + \
#           (ten * rim_num[10]) + (five * rim_num[5]) + (one * rim_num[1])
#     return res
#
# print func(num)
