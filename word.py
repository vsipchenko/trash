import re

st = '11. 222 3333 444 1! 2 3, 555555? 4 3 555555, 44443 32'


def make_lst(string):
    result_lst = re.findall(r'\W\ ', string)
    for i in result_lst:
        string = string.replace(i, ' ')

    return string.split(' ')


def display_longest(string):
    lst = make_lst(string)
    words = ['']
    for i in lst:
        if len(i) > len(max(words)):
            words = [i]
        elif len(i) == len(max(words)):
            words.append(i)
    print words
    return words


display_longest(st)
