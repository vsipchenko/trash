import random


class Device(object):
    state = False

    def __getattribute__(self, name, *args):
        if name == 'state' or name == 'change_state':
            return object.__getattribute__(self, name, *args)
        if not self.state:
            print "Torn on device please, it is not working!"
        return object.__getattribute__(self, name, *args)

    def get_state(self):
        return self.state

    def change_state(self):
        if self.state:
            self.state = False
        else:
            self.state = True
        return self.state


class ReproducingSoundDevice(Device):
    volume_range = range(0, 101)
    current_volume = 50

    def set_volume(self, value):
        if value not in self.volume_range:
            print 'Value not in range'
        else:
            self.current_volume = value
        return self.current_volume

    def off_sound(self):
        self.current_volume = 0


class InternetUsingDevice(Device):
    connection_state_chance = [True, False, True, True]
    connection_state = connection_state_chance[random.randint(0, len(connection_state_chance)) - 1]

    def check_connection_state(self):
        return self.connection_state

    def reload_device(self):
        self.connection_state = connection_state_chance[random.randint(0, len(connection_state_chance)) - 1]
        return self.connection_state


class DisplayUsingDevice(Device):
    brights_range = range(1, 101)
    current_brights = 50

    def set_brights(self, value):
        if value not in self.brights_range:
            print 'Not in range'
        else:
            self.current_brights = value
        return self.current_brights


class TV(DisplayUsingDevice, ReproducingSoundDevice):
    channels = {1: 'Inter', 2: '1+1', 3: 'ICTV', 4: 'STB'}
    current_channel = 1

    def next_channel(self):
        if not self.current_channel == len(self.channels):
            self.current_channel += 1
        else:
            self.current_channel = 1
        return self.current_channel

    def prev_channel(self):
        if not self.current_channel == 1:
            self.current_channel -= 1
        else:
            self.current_channel = len(self.channels)
        return self.current_channel

    def get_channel_name(self):
        return self.channels.get(self.current_channel)


class Radio(ReproducingSoundDevice):
    channels = range(0, 150)
    current_channel = 0

    def set_channel(self, value):
        if value in channels:
            self.current_channel = value
        return self.current_channel


class NewsDisplayDevice(DisplayUsingDevice):
    news_archive = ['google.com', 'vk.com/news', 'some-site.ua/news']

    def get_news_archive(self):
        return self.news_archive

    def add_resource(self, value):
        self.news_archive.append(value)
        return self.news_archive

    def show_news(self, value):
        return "I'll try to connect to {}".format(value)
